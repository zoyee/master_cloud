#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyhdb
import pandas as pd
import sklearn
import numpy as np
import datetime
import os
import time
from os import listdir
from os.path import isfile, join

from sklearn.linear_model import LinearRegression
from sklearn import preprocessing, svm
from sklearn.model_selection import cross_validate,train_test_split

hana_express_svc = os.environ.get('HANA_EXPRESS_SVC')
hana_express_port = os.environ.get('HANA_EXPRESS_PORT')
hana_express_user = os.environ.get('HANA_EXPRESS_USER')
hana_express_password = os.environ.get('HANA_EXPRESS_PASSWORD')
dataservice_name = os.environ.get('DATASERVICE_NAME')

check = True
while check==True:
    response = os.system("ping -c 1 " + dataservice_name)
    if response == 0:
        print("dataservice is up!")
    else:
        check = False
    time.sleep (10)

connection = pyhdb.connect(
    host=hana_express_svc,
    port=hana_express_port,
    user=hana_express_user,
    password=hana_express_password
)

# get connection with database HANA
cursor = connection.cursor()
name =[]

onlyfiles = [f for f in listdir('/tmp/csv-files') if isfile(join('/tmp/csv-files', f))]

# Create and delete table in hana database
for f in onlyfiles:
    name_of_aktie = str.replace(f,'.csv','')
    chars = {'ö':'oe','ä':'ae','ü':'ue'}     
    for char in chars:
        name_of_aktie = name_of_aktie.replace(char,chars[char])
    name_of_aktie = str.replace(name_of_aktie,'-','')
    name_of_aktie = str.replace(name_of_aktie,'.ON','ON')
    name_of_aktie = str.replace(name_of_aktie,'.','')
    name_of_aktie = str.replace(name_of_aktie,'(VW)','')
    name_of_aktie = str.replace(name_of_aktie,' ','')
    print(name_of_aktie)
    name.append(name_of_aktie)

# create forecast table
try:
    cursor.execute("DROP TABLE forecast;")
    print("Table forecast delete")
except:
    print("Database not exist")
cursor.execute("CREATE TABLE forecast (StockName varchar (255), FirstWeek float , SecoundWeek float , ThirdWeek float , FourthWeek float ); ")

for akName in name :
    cursor.execute("SELECT * FROM " + akName + ";" )    
    print(akName)
    #get header names
    #https://enlight.nyc/projects/stock-market-prediction/
    columns = [column[0] for column in cursor.description]
    print(columns)
    a = cursor.fetchall()
    # save in panda data frame 
    data = pd.DataFrame(a)
    # change header in data frame
    data.columns =columns
    print(data)
    #get spezific column for analyis
    data = data[['CLOSE']]
    print(data)
    forecast_out = int(4) # predicting week into future
    data['Prediction'] = data[['CLOSE']].shift(-forecast_out) #  label column with data shifted 4 units up

    X = np.array(data.drop(['Prediction'], 1))
    X = preprocessing.scale(X)

    X_forecast = X[-forecast_out:] # set X_forecast equal to last 4
    X = X[:-forecast_out] # remove last 30 from X

    y = np.array(data['Prediction'])
    y = y[:-forecast_out]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2)
    #Training
    clf = LinearRegression()
    clf.fit(X_train,y_train)
    # Testing
    confidence = clf.score(X_test, y_test)
    print("confidence: ", confidence)
    forecast_prediction = clf.predict(X_forecast)
    print(forecast_prediction)
    
    cursor.execute("INSERT INTO forecast (StockName, FirstWeek, SecoundWeek, ThirdWeek, FourthWeek) VALUES ('"+ akName +"'," +str(forecast_prediction[0])+ "," +str(forecast_prediction[1])+ ","+str(forecast_prediction[2])+ ","+str(forecast_prediction[3])+");")
    connection.commit()

connection.close()