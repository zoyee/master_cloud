# Check if hana database is ready
python /app/check-hana.py

# Function get and parse ticker data to json
function getTickerData () {
    aktieTickerSymbol=$1
    aktieName=$2

    curl -i -s -H "Accept: application/json" "https://www.alphavantage.co/query?function=TIME_SERIES_WEEKLY&symbol="${aktieTickerSymbol}"&interval=1min&apikey="${ALPHA_VANTAGE_FREE_KEY}"" > /data/${aktieName}.json
    sleep 30
    sed -e '1,11d' /data/${aktieName}.json > /data/${aktieName}temp.json
    rm -rf /data/${aktieName}.json
    mv /data/${aktieName}temp.json /data/${aktieName}.json
    sed -i 's/Weekly Time Series/WeeklyTimeSeries/g' /data/${aktieName}.json
    sed -i 's/1. open/open/g' /data/${aktieName}.json
    sed -i 's/2. high/high/g' /data/${aktieName}.json
    sed -i 's/3. low/low/g' /data/${aktieName}.json
    sed -i 's/4. close/close/g' /data/${aktieName}.json
    sed -i 's/5. volume/volume/g' /data/${aktieName}.json

    jq -r '"date,open,high,low,close,volume",(.WeeklyTimeSeries | keys[] as $k | "\($k),\(.[$k] | .open),\(.[$k] | .high),\(.[$k] | .low),\(.[$k] | .close),\(.[$k] | .volume)")' /data/${aktieName}.json > /data/csv-files/${aktieName}.csv 
}

# Function remove data if this is invalid
function removeInvalidData (){
    aktieName=$1

    rm -rf /data/csv-files/${aktieName}.csv 
    echo "${aktieName} data is broken"
}

# Get data from alphavantage
while read aktie || [ -n "$aktie" ]; do
aktieTickerSymbol=`echo "$aktie" | cut -d"," -f 2`
aktieName=`echo "$aktie" | cut -d"," -f 1` 
echo ${aktieTickerSymbol}
aktieName=`echo ${aktieName//[[:blank:]]/}`
echo ${aktieName}
getTickerData ${aktieTickerSymbol} ${aktieName} || getTickerData ${aktieTickerSymbol} ${aktieName} || removeInvalidData ${aktieName} 
rm -rf /data/${aktieName}.json
done </app/dax-aktien.txt

# Update hana database
python /app/update-data-in-database.py