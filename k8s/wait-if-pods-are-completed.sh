#!/bin/bash
while true; do
   POD_STATUS=$(kubectl -n default describe pods -l k8s-app=dataservice  | grep ^Status: | head -1 | awk '{print $2}' | tr -d '\n') 
   if [ "$POD_STATUS" == "Succeeded" ]
    then 
     echo "pod is Completed" 
     exit 0
   else 
     echo "waiting for pod" && sleep 5  
   fi 
done