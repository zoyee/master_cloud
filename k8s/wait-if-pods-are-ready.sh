#!/bin/bash
while true; do
   POD_STATUS=$(kubectl -n default describe pods -l k8s-app=hana-express  | grep ContainersReady | awk '{print $2}' | tr -d '\n') 
   if [ "$POD_STATUS" == "True" ]
    then 
     echo "pod is ready" 
     exit 0
   else 
     echo "waiting for pod" && sleep 5  
   fi 
done