#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyhdb
import os
import time
import sys
from os import listdir
from os.path import isfile, join

reload(sys)
sys.setdefaultencoding('utf8')
hana_express_svc = os.environ.get('HANA_EXPRESS_SVC')
hana_express_port = os.environ.get('HANA_EXPRESS_PORT')
hana_express_user = os.environ.get('HANA_EXPRESS_USER')
hana_express_password = os.environ.get('HANA_EXPRESS_PASSWORD')

check = True
while check:
    try:
        connection = pyhdb.connect(
            host=hana_express_svc,
            port=hana_express_port,
            user=hana_express_user,
            password=hana_express_password
            )
        if(connection.isconnected()):
           check=False
           connection.close()
    except:
        print("Connection to hana database refused")
        time.sleep (10)