# 0.0.0.0:5000 als inscure Registry hinzufügen für docker for mac

update_docker_images_minikube: check_minikube_is_up create_registry_if_not_exists tag_docker_images port_forwarding push_docker_images
	@eval $$(minikube docker-env);\
	docker pull 127.0.0.1:5000/hana-express:latest
	@eval $$(minikube docker-env);\
	docker pull 127.0.0.1:5000/dataservice:latest
	@eval $$(minikube docker-env);\
	docker pull 127.0.0.1:5000/forecastservice:latest
	@eval $$(minikube docker-env);\
	docker images

check_minikube_is_up:
	@if [ "$(shell minikube status | grep Stopped)" == "host: Stopped" ]; then\
	    echo Minikube is not running;\
        minikube start;\
    fi

create_registry_if_not_exists:
	kubectl apply -f k8s/kube-registry.yaml -n kube-system

tag_docker_images:
	docker tag hana-express:latest 0.0.0.0:5000/hana-express:latest
	docker tag master_cloud_dataservice:latest 0.0.0.0:5000/dataservice:latest
	docker tag master_cloud_forecastservice:latest 0.0.0.0:5000/forecastservice:latest

port_forwarding:
	$(eval registry_pod := $(shell kubectl get po -n kube-system | grep kube-registry-v0 | awk '{print $$1;}'))
	osascript -e 'tell app "Terminal" to do script "kubectl port-forward --namespace kube-system $(registry_pod) 5000:5000"'

push_docker_images:
	docker push 0.0.0.0:5000/hana-express:latest
	docker push 0.0.0.0:5000/dataservice:latest
	docker push 0.0.0.0:5000/forecastservice:latest

clean_dockerimages:
	docker rmi \
	@eval $$(docker images | grep "^<none>" | awk "{print $3}")
	docker images -f "dangling=true" -q
