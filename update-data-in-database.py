#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyhdb
import os
from os import listdir
from os.path import isfile, join

import sys
reload(sys)
sys.setdefaultencoding('utf8')
hana_express_svc = os.environ.get('HANA_EXPRESS_SVC')
hana_express_port = os.environ.get('HANA_EXPRESS_PORT')
hana_express_user = os.environ.get('HANA_EXPRESS_USER')
hana_express_password = os.environ.get('HANA_EXPRESS_PASSWORD')

connection = pyhdb.connect(
    host=hana_express_svc,
    port=hana_express_port,
    user=hana_express_user,
    password=hana_express_password
)

cursor = connection.cursor()

onlyfiles = [f for f in listdir('/data/csv-files') if isfile(join('/data/csv-files', f))]

# Create and delete table in hana database
for f in onlyfiles:
    name_of_table = str.replace(f.encode("utf-8"),'.csv','')
    chars = {'ö':'oe','ä':'ae','ü':'ue'}     
    for char in chars:
        name_of_table = name_of_table.replace(char,chars[char])
    name_of_table = str.replace(name_of_table,'-','')
    name_of_table = str.replace(name_of_table,'.ON','ON')
    name_of_table = str.replace(name_of_table,'.','')
    name_of_table = str.replace(name_of_table,'(VW)','')
    print(name_of_table)
    try:
        cursor.execute("DROP TABLE "+ name_of_table + ";")
        print("Table "+ name_of_table +" delete")
    except:
        print("Database not exist")
    cursor.execute("CREATE TABLE "+ name_of_table + " (date DATE, open float, high float, low float, close float, volume float); ")
    
    print("Table "+ name_of_table +" create")
    
# https://github.com/SAP/PyHDB/issues/93
    try:
        cursor.execute("IMPORT FROM CSV FILE '/usr/sap/HXE/HDB90/work/"+ f.decode("utf-8") +"' INTO "+ name_of_table +";")
    except: 
        print("Database "+ name_of_table +" is create")
connection.close()