FROM alpine

# Install need packages
RUN apk update && apk add \
        curl \
        jq \
        python \
        python-dev \
        py-pip \
        build-base \
    && rm -rf /var/cache/apk/*

# Install pyton libary for database
RUN pip install pyhdb

RUN mkdir app

# Create non-root user
RUN addgroup -S master-group && \
    adduser -S master-user -G master-group --uid 1001

# Change the user
USER 1001

# Copy data to image
COPY --chown=master-user:master-group dax-aktien.txt /app
COPY --chown=master-user:master-group getData.sh /app
COPY --chown=master-user:master-group update-data-in-database.py /app
COPY --chown=master-user:master-group check-hana.py /app


RUN chmod 755 /app/getData.sh
RUN chmod 755 /app/update-data-in-database.py
RUN chmod 755 /app/check-hana.py

CMD ./app/getData.sh	
# CMD tail -f /dev/null