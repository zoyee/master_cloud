FROM python:3.7-alpine

RUN apk add --no-cache gcc g++ gfortran lapack-dev libffi-dev libressl-dev musl-dev && \
    mkdir scipy && cd scipy && \
    wget https://github.com/scipy/scipy/releases/download/v1.3.2/scipy-1.3.2.tar.gz && \
    tar -xvf scipy-1.3.2.tar.gz && \
    cd scipy-1.3.2 && \
    python3 -m pip --no-cache-dir install .

# run all seperated because, if run all, it doesn't work
RUN pip install --upgrade cython
RUN pip install numpy
RUN pip install scikit-learn
RUN pip install pandas 
RUN pip install pyhdb 
RUN apk add --no-cache iputils

# Create app dir
RUN mkdir app

# Create non-root user
RUN addgroup -S master-group && \
    adduser -S master-user -G master-group --uid 1001

# Change the user
USER 1001

# Copy data to image
COPY --chown=master-user:master-group export-hana.py /app

CMD python /app/export-hana.py
#ENTRYPOINT /bin/sh
